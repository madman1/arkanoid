﻿var reflectrdFromBottom = false;   // this flag against bug when ball reflects from bottom and in that momment 
                                    // we move paddle on ball. so it sticks. 
var finishText = "";

var context;
var speed = 650;
var requestAnimationFrame;
var ball;
var paddle;
var then;
var lifesNumber;

var Blocks = [];

addEventListener("mousemove", Mouse_MouseMove, false);


//initialize
window.onload = function ()
{
    InitializeNewGame();
}

function InitializeNewGame()
{
    var canv = document.getElementById("canv");
    canv.width = document.documentElement.clientWidth - 30;
    canv.height = document.documentElement.clientHeight - 30;
    //canv.width = window.innerWidth - 30;
    //canv.height = window.innerHeight - 30;
    context = canv.getContext('2d');
    requestAnimationFrame = window.requestAnimationFrame ||
        window.webkitRequestAnimationFrame ||
        window.msRequestAnimationFrame ||
        window.mozRequestAnimationFrame;

    //ball = new Ball(canv.width / 2, canv.height / 1.5);
    ball = new Ball(300, 300);
    ball.velocity.x = -speed * Math.cos(ball.angle * Math.PI / 180);
    ball.velocity.y = -speed * Math.sin(ball.angle * Math.PI / 180);

    for (var b = 10; b < canv.height / 2; b += 30) {
        for (var i = 30; i < canv.width - 100; i += 120) {
            Blocks.push(new Block(i, b));
        }
    }

    paddle = new Block(canv.width / 2, canv.height - 15);
    paddle.color = "rgb(0, 255, 50)";
    paddle.height = 15;
    paddle.width = 100;

    lifesNumber = 3;

    then = Date.now();
    Main();
}

function Main()
{
    var now = Date.now();
    var delta = now - then;

    Update(delta / 1000);

    finishText = "";
    if (Blocks.length == 0)
    {
        finishText = "You win! Start new game?";
    }
    
    if (lifesNumber <= 0)
    {
        finishText = "You loose. Start new game?";
    }

    if (finishText.length > 0)
    {
        if (confirm(finishText)) {
            InitializeNewGame();
            return;
        }
        else
            return;
    }

    CheckTopAndBottom();
    CheckLeftAndRight();
    CheckIntersections();
 
    DrawScreen();
    DrawBlocks();
    DrawNumberOfLifes();
    DrawPaddle();
    DrawCircle();

    then = now;

    // Request to do this again ASAP
    requestAnimationFrame(Main);    // start new game cicle
}

function Update(delta)
{
    ball.position = ball.position.add(ball.velocity.multiply(delta));
}

function DrawNumberOfLifes()
{
    context.fillStyle = "black";
    context.font = "bold 16px Arial";
    context.fillText(lifesNumber.toString(), 10, 20);
}

function DrawBlocks()
{
    for (var i = 0; i < Blocks.length; i++) {
        var b = Blocks[i];
        context.fillStyle = b.color;
        context.fillRect(b.position.x, b.position.y, b.width, b.height);
    }
}

function DrawScreen()
{
    context.clearRect(0, 0, canv.width, canv.height);
}

function DrawCircle()
{
    context.strokeStyle = ball.color;
    context.fillStyle = ball.color;
    context.beginPath();
    // add ball radius because it draws it form the center but all calculations are made from left top corner
    context.arc(ball.position.x + ball.radius, ball.position.y + ball.radius, ball.radius, 0, Math.PI * 2, true);
    context.closePath();
    context.stroke();
    context.fill();
}
function DrawPaddle()
{
    context.fillStyle = paddle.color;
    context.fillRect(paddle.position.x, paddle.position.y, paddle.width, paddle.height);
}

function CheckTopAndBottom()
{
    if ((ball.position.y) < 0)
    {
        ball.ReflectHorizontal();
        reflectrdFromBottom = false;
    }

    if ((ball.position.y) > canv.height - 30)
    {
        ball.ReflectHorizontal();
        lifesNumber--;
        reflectrdFromBottom = true;
    }
}

function CheckLeftAndRight()
{
    if ((ball.position.x) < 0)
    {
        ball.ReflectVertical();
        reflectrdFromBottom = false;
    }

    if ((ball.position.x) > canv.width - 30)
    {
        ball.ReflectVertical();
        reflectrdFromBottom = false;
    }
}

function CheckIntersections()
{
    if (!reflectrdFromBottom && isIntersect(paddle))
    {
        var ballPosRelativeToPaddle = ball.radius * 2 + ball.position.x - paddle.position.x - 1;
        var ballMaxPosRelativeToPaddle = ball.radius * 2 + paddle.width - 2;   // it is if ball intersects on the right corner of paddle
        var factor = ballPosRelativeToPaddle/ballMaxPosRelativeToPaddle;   // equal 0 if left on the left corner of paddle, and 1 if on the right
        var angle = (135 - factor * (135 - 45)) * Math.PI / 180;   // angle to horizontal. from right 0 deg to left 180 deg
        ball.velocity.y = -speed * Math.sin(angle);
        ball.velocity.x = speed * Math.cos(angle);
    }

    for (var i = 0; i < Blocks.length; i++)
    {
        if (isIntersect(Blocks[i]))
        {
            ball.ReflectHorizontal();
            Blocks.splice(i, 1);

            reflectrdFromBottom = false;
        }
    }
}


    function isIntersect(block)
    {
        return (ball.position.x < block.position.x + block.width &&
                       ball.position.x + 2 * ball.radius > block.position.x &&
                       ball.position.y < block.position.y + block.height &&
                       2 * ball.radius + ball.position.y > block.position.y);
    }
    

    function Mouse_MouseMove(e)
    {
        paddle.position.x = e.clientX;
    }

