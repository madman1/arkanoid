﻿function Ball(x, y)
{
    this.position = new Vector(x, y);
    this.velocity = new Vector(0, 0);
    this.radius = 15;
    this.angle = 30;
    this.color = "rgb(255, 100, 0)";
}

Ball.prototype.ReflectHorizontal = function () {
    this.velocity.y = -this.velocity.y;
}

Ball.prototype.ReflectVertical = function () {
    this.velocity.x = -this.velocity.x;
}